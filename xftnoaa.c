/*$
xftnoaa
Copyright (c) 2021 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/
/************************************
 * xftnoaa
 *
 * X リソースの Xft.antialias を ON/OFF する。
 * Wine などにおいて、一時的にアンチエイリアスの ON/OFF を変更したい場合に。
 * ※起動中の X11 でのみ有効。X11 を再起動すると元に戻る。
 *
 * <compile>
 * $ cc -O2 -o xftnoaa xftnoaa.c -lX11
 *
 * <usage>
 * $ ./xftnoaa [0|1|on|off]
 *
 * none: display help
 * 0 or off: antialias OFF
 * 1 or on:  antialias ON
 ************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>
#include <X11/Xresource.h>
#include <X11/Xatom.h>


/* リソース文字列にセット */

static char *_set_string(const char *src,int flag)
{
	char *buf,*pc,*end;
	int srclen,len1,len2;

	if(!src)
		pc = 0;
	else
		pc = strstr(src, "Xft.antialias:");

	if(!pc)
	{
		//新規

		buf = (char *)malloc(32);
		if(!buf) return 0;

		snprintf(buf, 32, "Xft.antialias:\t%d\n", flag);
	}
	else
	{
		//--- 置き換え

		srclen = strlen(src);

		buf = (char *)malloc(srclen + 4);
		if(!buf) return 0;

		//Xft.antialias 行を削除

		end = strchr(pc, '\n');
		if(end)
			end++;
		else
			end = (char *)(src + srclen);

		len1 = pc - src;
		len2 = (src + srclen) - end;

		memcpy(buf, src, len1);
		memcpy(buf + len1, end, len2);

		len1 += len2;

		//追加

		snprintf(buf + len1, 32, "Xft.antialias:\t%d\n", flag);
	}

	return buf;
}

/* プロパティにセット */

static void _set_prop(Display *disp,Window root,Atom prop,unsigned char *buf,long len)
{
	long max,size;
	int mode;

	max = XMaxRequestSize(disp) * 4;

	if(len <= max)
		XChangeProperty(disp, root, prop, XA_STRING, 8, PropModeReplace, buf, len);
	else
	{
		//分割
		
		XGrabServer(disp);

		mode = PropModeReplace;

		while(len)
		{
			size = (len > max)? max: len;
		
			XChangeProperty(disp, root, prop, XA_STRING, 8, mode, buf, size);

			buf += size;
			len -= size;
			mode = PropModeAppend;
		}

		XUngrabServer(disp);
	}
}

/* リソース処理 */

static void _proc(Display *disp,int flag)
{
	char *str,*buf;

	XrmInitialize();

	//ルートのリソース情報を文字列で取得
	// (NULL の場合あり)

	str = XResourceManagerString(disp);

	//値をセット

	buf = _set_string(str, flag);
	if(!buf) return;

	//ルートのプロパティにセット

	_set_prop(disp, RootWindow(disp, 0), XA_RESOURCE_MANAGER,
		(unsigned char *)buf, strlen(buf));

	free(buf);

	printf("set to %d\n", flag);
}

/* main */

int main(int argc,char **argv)
{
	Display *disp;
	int flag;

	//引数

	if(argc < 2)
	{
		//help

		puts("[Usage] xftnoaa [0|1|on|off]\n\n"
"Turn 'Xft.antialias' on/off.\n\n"
"0 or off: antialias off\n"
"1 or on : antialias on\n"
"\n"
"xftnoaa ver 1.0.0\n"
"Copyright (c) 2021 Azel"
);
		return 0;
	}
	else
	{
		if(strcasecmp(argv[1], "on") == 0)
			flag = 1;
		else if(strcasecmp(argv[1], "off") == 0)
			flag = 0;
		else
		{
			flag = atoi(argv[1]);
			flag = (flag != 0);
		}
	}

	//開く

	disp = XOpenDisplay(0);
	if(!disp)
	{
		printf("failed XOpenDisplay\n");
		return 1;
	}

	//設定

	_proc(disp, flag);

	//終了

	XCloseDisplay(disp);

	return 0;
}

