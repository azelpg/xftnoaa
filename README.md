# xftnoaa

動作環境: Linux、ほか unix 系 OS

### 日本語

X リソースの Xft.antialias を ON/OFF します。

Wine など、テキスト描画に Xft ライブラリを使っているソフトにおいて、
一時的にアンチエイリアスの有効/無効を変更したい場合に使います。

xrdb コマンドを使って変更することもできますが、
このプログラムでは、アンチエイリアスの項目のみを簡単に変更できます。

~~~
$ xftnoaa 0  # アンチエイリアス無効化
$ ...        # アンチエイリアス無効状態で実行したいコマンド
$ xftnoaa 1  # アンチエイリアスを元に戻す
~~~

- 永続的な変更ではないので、X11 を再起動すると元に戻ります。
- 実行した X11 上において、Xft を使うすべてのソフトウェアが影響を受けるので、
対象のソフトが終了した後は、元の状態に戻してください。

### English

Turn on/off Xft.antialias for the X resource.

It is used when you want to temporarily enable/disable antialiasing in software
that uses the Xft library for text drawing such as Wine.

You can change it using the xrdb command,
but this program makes it easy to change only the antialiasing item.

~~~
$ xftnoaa 0  # Antialiasing disabled
$ ...        # Commands you want to execute with antialiasing disabled
$ xftnoaa 1  # Antialiasing enabled
~~~

- It's not a permanent change, so restarting X11 will undo it.
- All software that uses Xft will be affected on the executed X11,
so please return to the original state after the target software is closed.
